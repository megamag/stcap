@echo off
echo type "commit" or "update"
cd C:\St\stcap\Polovinkina

echo hh

set GIT_PATH="C:\Program Files\Git\bin\git.exe"
set BRANCH = "master"

:P
set ACTION=
set /P ACTION=Action: %=%
if "%ACTION%"=="c" (
	%GIT_PATH% commit -am "Auto-committed on %date%"
	%GIT_PATH% tag -f pol
	%GIT_PATH% pull %BRANCH%
	%GIT_PATH% merge %BRANCH%
	%GIT_PATH% push %BRANCH%
	%GIT_PATH% push origin pol
)
if "%ACTION%"=="u" (
	%GIT_PATH% pull %BRANCH%
)
if "%ACTION%"=="exit" exit /b
goto P