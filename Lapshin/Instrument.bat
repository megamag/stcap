@echo off
echo type "commit" or "load"
cd "C:\Users\TEMP.PD\Desktop\stcap\Lapshin"

set GIT_PATH="C:\Program Files\Git\bin\git.exe"
set BRANCH = "master"

:P
set ACTION=
set /P ACTION=Action: %=%

if "%ACTION%"=="commit" (
	git add --all
	git commit -m "Auto-committed on %time% %date%"
 	git tag -a NewTagV2 -m "LapshinIS"
 	git pull
	git merge
	git push origin %BRANCH%
	git push origin --tags
)

if "%ACTION%"=="load" (
	git pull %BRANCH%
)