@echo off
echo type "commit" or "load" or "delete" or "exit"
cd C:\Users\TEMP.PD\Desktop\stcap\Borisyuk

set GIT_PATH="C:\Program Files\Git\bin\git.exe"
set BRANCH = "master"

:P
set ACTION=
set /P ACTION=Action: %=%

if "%ACTION%"=="commit" (
	git add --all
	git commit -m "Auto-committed on %time% %date%"
 	git tag -a Ilys_uk -m "Ilys"
 	git pull
	git merge
	git push origin %BRANCH%
	git push origin --tags
)

if "%ACTION%"=="load" (
	git pull %BRANCH%
)

if "%ACTION%"=="delete" (
		git tag -d Smesh
		git push origin --delete Smesh
)

if "%ACTION%"=="exit" exit /b
goto P