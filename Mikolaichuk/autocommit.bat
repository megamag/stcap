@echo off

:P
set /P ACTION="Action (pull, push, exit): "

if "%ACTION%"=="pull" (
	git pull --tags
	git merge
)

if "%ACTION%"=="push" (
	notepad "mikolaichukk.txt"
	git commit -am "This is auto-commit by NMikolaichuk"
	git tag -a m_v.0.9.7 -m "mikolaichuk version tag"
	git pull
	git merge
	git push origin m_v.0.9.7
)

if "%ACTION%"=="exit" (
	exit /b
)
goto P