@echo off
echo type "delete" or "edit" or "commit" or "update" or "exit"
cd "C:\University\4 course\MegaMag\stcap\Sorokina"

set GIT_PATH="C:\Program Files\Git\bin\git.exe"
set BRANCH = "master"

:P
set ACTION=
set /P ACTION=Action: %=%

if "%ACTION%"=="delete" (
	git tag -d esVolt
	git push origin --delete esVolt
)

if "%ACTION%"=="edit" (
	notepad "Popytka5.txt"
)

if "%ACTION%"=="commit" (
	%GIT_PATH% add -A
  	%GIT_PATH% commit -am "File changed|update on %date%"
	%GIT_PATH% tag -a esVolt -m "EsVolt" %BRANCH%
	%GIT_PATH% pull %BRANCH%
	%GIT_PATH% merge %BRANCH%
	%GIT_PATH% push %BRANCH%
	%GIT_PATH% push origin --tags
)

if "%ACTION%"=="update" (
	%GIT_PATH% pull %BRANCH%
)

if "%ACTION%"=="exit" exit /b
goto P