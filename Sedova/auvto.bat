@echo off
echo type "commit" or "update"
cd "C:\Users\TEMP.PD.000\Desktop\stcap\Sedova"

set GIT_PATH="C:\Program Files\Git\bin\git.exe"
set BRANCH = "master"

:P
set ACTION=
set /P ACTION=Action: %=%
if "%ACTION%"=="c" (
  %GIT_PATH% commit -am "Sedova: some changes"
	%GIT_PATH% tag -f sed
	%GIT_PATH% pull %BRANCH%
	%GIT_PATH% merge %BRANCH%
	%GIT_PATH% push %BRANCH%
	%GIT_PATH% push origin sed
)
if "%ACTION%"=="u" (
	%GIT_PATH% pull %BRANCH%
)
if "%ACTION%"=="exit" exit /b
goto P