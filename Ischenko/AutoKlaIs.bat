@echo off
echo type "commit" or "update"
cd "D:\repozitory\stcap\Ischenko"

set GIT_PATH = "C:\Program Files\Git\bin\git.exe"
set BRANCH = "master"

:P
set ACTION = 
set /P ACTION=Action: %=%
if "%ACTION%"=="c" (
  %GIT_PATH% git add -A
	%GIT_PATH% git commit -am "Auto-committed on %date%"
    %GIT_PATH% git tag -a ABCd -m "Dmitry" %BRANCH%
	%GIT_PATH% git pull %BRANCH%
	%GIT_PATH% git merge %BRANCH%
	%GIT_PATH% git push origin %BRANCH%
    %GIT_PATH% git push origin --tags
)
if "%ACTION%"=="u" (
	%GIT_PATH% git pull %BRANCH%
)
if "%ACTION%"=="d" (
	git tag -d v0.1D
	git push origin --delete v0.1D
)
if "%ACTION%"=="exit" exit /b
goto P