cd /d %~dp0\..
git commit -a --author "AsriFox <lynx42@ro.ru>" -m "AsriFox updated this with a script on %date%"
git tag -a afv1.2 -m "AsriFox code v1-1"
git pull origin
git merge
git push origin master afv1.2
git push origin --tags